#!/usr/bin/env python

import requests
import matplotlib.pyplot as plt
import config
import time
from lxml import html

url = 'https://www.waze.com/live-map/api/user-drive?geo_env=row'

wayBackToo = True
generateSvg = False

def getWazeData(from_, to):
    data = {
        'from': from_,
        'to': to,
        'nPaths': 1,
        'useCase': 'LIVEMAP_PLANNING',
        'endTime': int(time.time() * 1_000) + 3_600 * 1_000 * 24,
        'interval': 5 # in minutes. Optional (default: 15)
    }

    response = requests.post(url, json = data)
    try:
        data = response.json()
    except:
        text = response.text
        tree = html.fromstring(text)
        if tree.xpath('//title')[0].text == '429':
            print('Too many requests!')
            exit(1)
        else:
            print(f'Please report: {text}')
            exit(1)

    etaHistograms = data['alternatives'][0]['response']['etaHistograms']
    return etaHistograms

etaHistograms = getWazeData(config.from_, config.to)
if wayBackToo:
    wayBackEtaHistograms = getWazeData(config.to, config.from_)

X = range(len(etaHistograms))
XLabels = []
YWayOut = []
iterator = etaHistograms if not wayBackToo else zip(etaHistograms, wayBackEtaHistograms, strict = True)
if wayBackToo:
    YWayBack = []

for etaHistogram_s in iterator:
    if wayBackToo:
        etaHistogram, wayBackEtaHistogram = etaHistogram_s
        XLabel = etaHistogram['text'].split('-')[0] + '-' + wayBackEtaHistogram['text']
    else:
        etaHistogram = etaHistogram_s
        XLabel = etaHistogram['text']
    XLabel = ' - '.join(XLabel.split('-'))
    XLabels += [XLabel]

    YWayOut += [etaHistogram['routeLengthInMinutes']]
    if wayBackToo:
        YWayBack += [wayBackEtaHistogram['routeLengthInMinutes']]

plt.grid()

plt.xticks(X, XLabels, rotation = 'vertical')

plt.plot(YWayOut, label = 'Way out')
if wayBackToo:
    plt.plot(YWayBack, label = 'Way back')

xLabelParts = ['Departure (way out)', 'arrival time']
if wayBackToo:
    xLabelParts.insert(1, 'departure (way back)')
xLabel = ' - '.join(xLabelParts)
plt.xlabel(xLabel)

plt.legend()
plt.ylabel('Trip time in minutes')
plt.title('Waze estimated trip time')

# To fit x-axis legend.
plt.tight_layout()

if generateSvg:
    plt.savefig('waze_traffic.svg', transparent = True)
else:
    plt.show()
