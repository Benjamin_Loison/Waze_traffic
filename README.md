# Waze traffic

Web-scrap `TRAFFIC` of [the Waze website live map](https://www.waze.com/live-map).

Don't forget to update `from` and `to` trip locations in `config.py`.

![matplotlib display of the Waze estimated trip time along the day](waze_traffic.svg)
